﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Notigram.Control;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

namespace Notigram.Models
{
    public class Service
    {
        string sContentType = "application/json";

        public async Task<Control.CrearReporte> UploadImage(string title, string idPost, MemoryStream stream)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers
                    .AuthenticationHeaderValue("Bearer", Preferences.Get("token",""));
                var formData = new MultipartFormDataContent();
                HttpContent titulo = new StringContent(title);
                HttpContent IdPost = new StringContent(idPost);
                HttpContent IdAuthor = new StringContent(Preferences.Get("user_id", ""));
                byte[] img = stream.ToArray();
                HttpContent image = new ByteArrayContent(img);
                image.Headers.ContentType = MediaTypeHeaderValue.Parse("image/png");
                formData.Add(titulo, "title");
                formData.Add(IdPost, "post");
                formData.Add(IdAuthor, "author");
                formData.Add(image, "file", titulo + ".png");
                var response = await client.PostAsync(Constant.Constants.urlMedia, formData);
                if (response.IsSuccessStatusCode)
                {
                    var result = (response.Content.ReadAsStringAsync().Result);
                    var jsonresult = JsonConvert.DeserializeObject<Control.CrearReporte>(result);
                    //Console.WriteLine("json al subir la imagen" + jsonresult.id);
                    return jsonresult;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public async Task<bool> PutReporte(string featured_media, string id)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new
                    AuthenticationHeaderValue("Bearer", Preferences.Get("token", ""));
                JObject jObject = new JObject();
                if(!String.IsNullOrEmpty(featured_media) && !String.IsNullOrEmpty(id))
                {
                    jObject.Add("id", id);
                    jObject.Add("featured_media", featured_media);
                    var oTaskPostAsync = await client.PutAsync(Constant.Constants.urlReporte + "/" + id,
                        new StringContent(jObject.ToString(), Encoding.UTF8, sContentType));
                    if (oTaskPostAsync.IsSuccessStatusCode)
                    {
                        return true;
                    }
                    else
                        return false;
                }
                else
                    return false;
            }
            else
                return false;
        }
        public async Task<Control.CrearReporte> CreateReport(string title, string content,string featured_media,
            string ciudad_r, string tipo_r, string id)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.
                    Headers.AuthenticationHeaderValue("Bearer", Preferences.Get("token",""));
                JObject oJsonObject = new JObject();
                if(!String.IsNullOrEmpty(title) && !String.IsNullOrEmpty(content) && String.IsNullOrEmpty(featured_media)
                    && !String.IsNullOrEmpty(ciudad_r) && !String.IsNullOrEmpty(tipo_r) && String.IsNullOrEmpty(id))
                {
                    oJsonObject.Add("title",title);
                    oJsonObject.Add("content",content);
                    oJsonObject.Add("author",Preferences.Get("user_id", ""));
                    oJsonObject.Add("ciudad_r",ciudad_r);
                    oJsonObject.Add("tipo_r",tipo_r);
                }
                else
                {
                    oJsonObject.Add("id",id);
                    oJsonObject.Add("featured_media",featured_media);
                }
                var oTaskPostAsync = await client.PostAsync(Constant.Constants.urlReporte,
                    new StringContent(oJsonObject.ToString(), Encoding.UTF8, sContentType));
                if(oTaskPostAsync.IsSuccessStatusCode)
                {
                    var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                    var jsonResul = JsonConvert.DeserializeObject<CrearReporte>(result);
                    Console.WriteLine("json al subir el reporte "+jsonResul.id);
                    return jsonResul;
                }
                else
                    return null;
            }
            else
            {
                return null;
            }
        }

        public async Task<bool> ValidateToken()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer",
                    Preferences.Get("token", ""));
                var oTaskPostAsync = await client.PostAsync(Constant.Constants.urlValidateToken,
                    new StringContent("", Encoding.UTF8, sContentType));
                Console.WriteLine("token " +Preferences.Get("token",""));
                Console.WriteLine("Codigo de Token "+oTaskPostAsync.StatusCode.GetHashCode());
                if(oTaskPostAsync.StatusCode.GetHashCode() == 200)
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }

        public async Task<bool> CreateUser(string userName, string email, string password)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                JObject oJsonObject = new JObject();
                if(!String.IsNullOrEmpty(email)||!String.IsNullOrEmpty(password)
                    || !String.IsNullOrWhiteSpace(userName))
                {
                    oJsonObject.Add("username", userName);
                    oJsonObject.Add("email", email);
                    oJsonObject.Add("password", password);
                }
                else
                {
                    return false;
                }
                var oTaskPostAsync = await client.PostAsync(Constant.Constants.urlRegister,
                    new StringContent(oJsonObject.ToString(), Encoding.UTF8,sContentType));
                Console.WriteLine(oTaskPostAsync.StatusCode.GetHashCode());
                if (oTaskPostAsync.StatusCode.GetHashCode() == 200)
                {
                    return true;
                }
                else
                    return false;
            }
            return false;
        }
        public async Task<Control.Usuario> Login(string userName, string password)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                JObject oJsonObject = new JObject();
                if (!String.IsNullOrEmpty(userName) || !String.IsNullOrEmpty(password))
                {
                    oJsonObject.Add("username",userName);
                    oJsonObject.Add("password", password);
                }
                var oTaskPostAsync = await client.PostAsync(Constant.Constants.urlLogin,
                    new StringContent(oJsonObject.ToString(), Encoding.UTF8, sContentType));
                Console.WriteLine("status code create user "+oTaskPostAsync.StatusCode.GetHashCode()+ 
                    " tipo de error "+oTaskPostAsync.StatusCode);
                if (oTaskPostAsync.StatusCode.GetHashCode() == 403)
                {
                    return null;
                }
                else
                {
                    var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                    var jsonResul = JsonConvert.DeserializeObject<Usuario>(result);
                    return jsonResul;
                }
            }
            return null;
        }

        public async Task<List<AuthorName>> GetReporteros(int page)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsnc = await client.GetAsync(Constant.Constants.urlUsuario+page);
                //Console.WriteLine("Hola");
                var result = (oTaskPostAsnc.Content.ReadAsStringAsync().Result);
                var jsonResul = JsonConvert.DeserializeObject<List<AuthorName>>(result);
                return jsonResul;
            }
            else
                return null;
        }

        public async Task<List<TipoReporte>> GetReportes()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.utlTipoReporte);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var jsonResul = JsonConvert.DeserializeObject<List<TipoReporte>>(result);
                return jsonResul;
            }
            else
                return null;
        }

        public async Task<List<CiudadesReporte>> GetCiudades()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlCiudad);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var jsonResul = JsonConvert.DeserializeObject<List<CiudadesReporte>>(result);
                return jsonResul;
            }
            else
                return null;
        }

        public async Task<bool> PostComentarios(string post, string author_email, 
            string author_name, string content, string parent)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                
                JObject oJsonObject = new JObject();
                if (parent == null)
                {
                    oJsonObject.Add("post",post);
                    oJsonObject.Add("author_email", author_email);
                    oJsonObject.Add("author_name", author_name);
                    oJsonObject.Add("content", content);
                }
                else
                {
                    oJsonObject.Add("post", post);
                    oJsonObject.Add("author_email", author_email);
                    oJsonObject.Add("author_name", author_name);
                    oJsonObject.Add("content", content);
                    oJsonObject.Add("parent", parent);
                }
                var oTaskPostAsync = await client.PostAsync(Constant.Constants.urlComentarios.Replace("{}", ""),
                    new StringContent(oJsonObject.ToString(), Encoding.UTF8, sContentType));
                Console.WriteLine(oTaskPostAsync.IsSuccessStatusCode);
                return true;
            }
            else
                return true;
        }

        public async Task<List<Comentarios>> GetComentarios(string idPost) 
        {
            if (CheckInternetService.IsInternet())
            {
                if (idPost != null)
                {

                    var client = new HttpClient();
                    var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlComentarios.Replace("{}",idPost));
                    Console.WriteLine(Constant.Constants.urlComentarios.Replace("{}", idPost));
                    var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                    var jsonResul = JsonConvert.DeserializeObject<List<Comentarios>>(result);
                    return jsonResul;
                }
                else
                    return null;
                
            }
            else 
                return null;
        }

        public async Task<List<Menu>> GetCityMenu(String idCity)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                Console.WriteLine(Constant.Constants.urlCategoriesParent+"parent="+idCity);
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlCategoriesParent+"parent="+idCity);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var jsonResult = JsonConvert.DeserializeObject<List<Menu>>(result);
                if (oTaskPostAsync.IsSuccessStatusCode)
                {
                    return jsonResult;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public async Task<JObject> GetMenuR()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlMenuCities);
                if (oTaskPostAsync.IsSuccessStatusCode)
                {
                    var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                    //EL objeto
                    var jsonResul = JsonConvert.DeserializeObject<JObject>(result);
                    return jsonResul;
                }
                else
                    return null;
            }
            else
                return null;
        }

        public async Task<List<Menu>> GetMenu()
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlCategories);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var jsonResul = JsonConvert.DeserializeObject<List<Menu>>(result);
                Console.WriteLine("Codigo "+oTaskPostAsync.IsSuccessStatusCode);
                return jsonResul;
                
            }
            else
                return null;
        }

        public async Task<List<Obituarios>> GetObituarios(int page)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(Constant.Constants.urlObituario+"&page="+page);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var JsonResul = JsonConvert.DeserializeObject<List<Obituarios>>(result);
                return JsonResul;
            }
            else
                return null;
        }

        public async Task<List<Horoscopos>> GetHoroscopos(int page)
        {
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                var oTaskPostasync = await client.GetAsync(Constant.Constants.urlHoroscopo+"&page="+page);
                var resul = (oTaskPostasync.Content.ReadAsStringAsync().Result);
                var jsonResul = JsonConvert.DeserializeObject<List<Horoscopos>>(resul);
                return jsonResul;
            }
            else
                return null;
        }

        public async Task<List<Noticias>> GetReporte(string idReporte, string idTipo, string page)
        {
            string url;
            if (CheckInternetService.IsInternet())
            {
                var client = new HttpClient();
                if (idReporte.Equals("0") && idTipo.Equals("0"))
                {
                    url = Constant.Constants.urlReporte + "?page=" + page;
                }
                else
                {
                    url = Constant.Constants.urlReporte + "?categories" + idReporte + "&page=" + page;
                }
                var oTaskPostAsync = await client.GetAsync(url);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var JsonResul = JsonConvert.DeserializeObject<List<Noticias>>(result);
                return JsonResul;
            }
            else
                return null;
        }

        public async Task<List<Noticias>> GetNoticiasHttp(String id, string page)
        {
            if (CheckInternetService.IsInternet())
            {
                List<Noticias> observable;
                if (id.Equals("0"))
                {
                    page = Constant.Constants.urlNoticias.Replace("{0}", "&page=" + page + "&");
                }
                else
                {
                    //Console.WriteLine(Constant.Constants.urlNoticias.Replace("{0}", "categories=" + id + "&"));
                    page = Constant.Constants.urlNoticias.Replace("{0}", "categories=" + id + "&page="+page+"&");
                }
                Console.WriteLine("url" + page);
                HttpWebRequest request = WebRequest.Create(page) as HttpWebRequest;
                String resp;
                HttpWebResponse response = request.GetResponse() as HttpWebResponse;
                using(StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    resp = reader.ReadToEnd();
                    var obj = JsonConvert.DeserializeObject<object>(resp);
                    string data = Convert.ToString(obj);
                    observable = JsonConvert.DeserializeObject<List<Noticias>>(data);
                }
                return observable;
            }
            else
                return null;
        }

        public async Task<List<Noticias>> GetNoticias(string id, string page)
        {
            if (CheckInternetService.IsInternet())
            {
                
                if (id.Equals("0"))
                {
                    page = Constant.Constants.urlNoticias.Replace("{0}", "&page="+page+"&");
                }
                else
                {
                    Console.WriteLine(Constant.Constants.urlNoticias.Replace("{0}", "categories=" + id + "&"));
                    page = Constant.Constants.urlNoticias.Replace("{0}", "categories=" + id + "&page="+page);
                }
                Console.WriteLine(page);
                var client = new HttpClient();
                var oTaskPostAsync = await client.GetAsync(page);
                var result = (oTaskPostAsync.Content.ReadAsStringAsync().Result);
                var JsonResul = JsonConvert.DeserializeObject<List<Noticias>>(result);
                return JsonResul;
            }
            else
                return null;
        }
    }
}
