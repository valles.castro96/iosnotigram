﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Notigram.Constant
{
    class Constants
    {
        public static string urlComentarios = "https://notigram.com/wp-json/wp/v2/comments{}";
        public static string urlCategories = "http://notigram.com/wp-json/custom/v2/categories";
        public static string urlNoticias = "https://notigram.com/wp-json/wp/v2/posts?{0}_embed";
        public static string urlCiudad = "https://notigram.com/wp-json/wp/v2/ciudad_r";
        public static string utlTipoReporte = "https://notigram.com/wp-json/wp/v2/tipo_r";
        public static string urlRegister = "https://notigram.com/wp-json/wp/v2/users/register";
        public static string urlLogin = "https://notigram.com/wp-json/jwt-auth/v1/token";
        public static string urlValidateToken = " https://notigram.com/wp-json/jwt-auth/v1/token/validate";
        public static string urlReporte = "https://notigram.com/wp-json/wp/v2/reportes";
        public static string urlMedia = "https://notigram.com/wp-json/wp/v2/media";
        public static string urlCategoriesParent = "https://notigram.com/wp-json/wp/v2/categories?";
        public static string urlMenuCities = "https://notigram.com/wp-json/custom/v2/menu-cities";
        public static string urlObituario = "https://notigram.com/wp-json/wp/v2/obituarios?_embed";
        public static string urlCiudades = "https://notigram.com/wp-json/wp/v2/ciudades/";
        public static string urlFuneraria = "https://notigram.com/wp-json/wp/v2/funeraria/";
        public static string urlHoroscopo = "https://notigram.com/wp-json/wp/v2/horoscopos?_embed";
        public static string urlUsuario = "https://notigram.com//wp-json/wp/v2/users?page=";

        public static string ciudades = @"["+
            "'Cuencamé',\n"+
            "'Durango',\n"+
            "'El Salto',\n"+
            "'Guadalupe Victoria',\n"+
            "'Santiago Papasquiaro',\n"+
            "'Vicente Guerrero',\n"+
            "'Chihuahua',\n"+
            "'Jiménez',\n"+
            "'Parral'\n"+
            "]";

        public static string content= @"[" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/aries.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/tauro.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/geminis.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/cancer.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/leo.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/virgo.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/libra.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/escorpio.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/sagitario.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/capricornio.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/acuario.png',\n" +
                "'https://notigram.com/wp-content/themes/notigram-2019/img/horoscopos/piscis.png'\n]";

        public static string imgs = @"["+
            "'https://imgsnotigram.s3.amazonaws.com/uploads/2020/05/secciones_obituarios.png',\n"+
            "'https://imgsnotigram.s3.amazonaws.com/uploads/2020/05/secciones_horoscopos.png'\n]";
        public static string[] otros = new string[] { "Obituarios", "Horóscopos" };

    }
}
