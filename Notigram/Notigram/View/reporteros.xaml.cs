﻿using Acr.UserDialogs;
using Notigram.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class reporteros : ContentPage
    {
        #region Variables
        IUserDialogs Dialogs;
        ReporterosViewModel reporterosViewModel;
        #endregion
        public reporteros()
        {
            InitializeComponent();
            Dialogs = UserDialogs.Instance;
            BindingContext = reporterosViewModel = new ReporterosViewModel();
            lsReportero.ItemAppearing += LsReportero_ItemAppearing;
            lsReportero.RefreshCommand = new Command(()=> 
            {
                lsReportero.IsRefreshing = true;
                reporterosViewModel.page = 1;
                reporterosViewModel.GetReportero();
                lsReportero.IsRefreshing = false;
            });
            lsReportero.ItemSelected += LsReportero_ItemSelected;
        }

        private void LsReportero_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lsReportero.SelectedItem = null;
        }

        private void LsReportero_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if(e.ItemIndex == (reporterosViewModel.Reporteros.Count) - 1)
            {
                reporterosViewModel.page++;
                reporterosViewModel.GetNewReportero();
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            reporterosViewModel.GetReportero();
        }

    }
}