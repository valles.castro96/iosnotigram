﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Notigram.View;
using Notigram.ViewModels;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace Notigram
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        #region Variables

        IUserDialogs Dialogs;
        private string id;
        Control.Noticias Noticias { get; set; }

        public int page = 1;
        public Command Command { get; set; }


        #endregion

        MainViewModel mainViewModel;
        public MainPage(string id, string title)
        {
            InitializeComponent();

            JArray jArray = JArray.Parse(Constant.Constants.ciudades);
            string ids = "";
            if (id.Equals("0"))
            {
                for (int i = 0; i < jArray.Count; i++)
                {
                    if (Preferences.ContainsKey(jArray[i].ToString()))
                    {
                        ids = ids + "," + Preferences.Get(jArray[i].ToString(), "");
                    }
                    //Console.WriteLine("Ciudades "+Preferences.Get(jArray[i].ToString(),""));
                }
            }

            BindingContext = mainViewModel = new MainViewModel(ids.Equals("")?id:ids);

            //Title = title.ToLower().Equals("inicio")?"":title;

            Dialogs = UserDialogs.Instance;
            this.id = id;
            lsNoticias.RefreshCommand = new Command(() =>
            {
                lsNoticias.IsRefreshing = true;
                mainViewModel.GetNoticias();
                lsNoticias.IsRefreshing = false;
            });
            lsNoticias.ItemSelected += ItemSelected;
            lsNoticias.ItemAppearing += LsNoticias_ItemAppearing;

            

        }

        public void Refresh(string id,string title) {
            JArray jArray = JArray.Parse(Constant.Constants.ciudades);
            string ids = "";
            if (id.Equals("0"))
            {
                for (int i = 0; i < jArray.Count; i++)
                {
                    if (Preferences.ContainsKey(jArray[i].ToString()))
                    {
                        ids = ids + "," + Preferences.Get(jArray[i].ToString(), "");
                    }
                    //Console.WriteLine("Ciudades "+Preferences.Get(jArray[i].ToString(),""));
                }
            }
            BindingContext = mainViewModel = new MainViewModel(ids.Equals("") ? id : ids);
            //Title = title;
            this.id = id;
            mainViewModel.page = 1;
            mainViewModel.id = ids.Equals("") ? id : ids;
            mainViewModel.GetNoticias();

        }

        private async void LsNoticias_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            
            if(e.ItemIndex == (mainViewModel.Noticias.Count -1 ))
            {
                mainViewModel.page ++;
                await mainViewModel.GetNewPage();
            }
        }

        async void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            this.Noticias = lsNoticias.SelectedItem as Control.Noticias;
            lsNoticias.SelectedItem = null;
            if (lsNoticias.SelectedItem != null)
            {
                await Navigation.PushAsync(new Noticia(this.Noticias), false);
            }
            

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            //Preferences.Remove("ciudades");
            //Console.WriteLine("Ciudades " + Preferences.Get("ciudades", ""));
            JArray jArray = JArray.Parse(Constant.Constants.ciudades);
            string ids = "";
            if (id.Equals("0"))
            {
                for (int i = 0; i < jArray.Count; i++)
                {
                    if (Preferences.ContainsKey(jArray[i].ToString()))
                    {
                        ids = ids + "," + Preferences.Get(jArray[i].ToString(), "");
                    }
                    //Console.WriteLine("Ciudades "+Preferences.Get(jArray[i].ToString(),""));
                }
            }
            mainViewModel.id = ids.Equals("") ? this.id : ids;
            mainViewModel.GetNoticias();
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new MenuD());
        }
    }
}
