﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Login : ContentPage
    {
        #region Variables
        IUserDialogs Dialogs;
        TapGestureRecognizer recognizer = new TapGestureRecognizer();
        TapGestureRecognizer recognizerReturn = new TapGestureRecognizer();
        #endregion

        public Login()
        {
            InitializeComponent();
            Dialogs = UserDialogs.Instance;
            recognizer.Tapped += Recognizer_Tapped;
            recognizerReturn.Tapped += RecognizerReturn_Tapped;
            Return.GestureRecognizers.Add(recognizerReturn);
            registrar.GestureRecognizers.Add(recognizer);
        }

        private void RecognizerReturn_Tapped(object sender, EventArgs e)
        {
            Application.Current.MainPage = new View.Menu();
        }

        private async void Recognizer_Tapped(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new SignUp(), false);
        }

        private async void Button_Clicked(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(usuario.Text) || !String.IsNullOrEmpty(contraseña.Text))
            {
                var emailPattern = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
                if (Regex.IsMatch(usuario.Text, emailPattern))
                {
                    LoginToNotigram();
                }
                else
                    await this.DisplayAlert("Notigram", "Ingrese un coreo valido","Aceptar");
            }
        }

        private async void LoginToNotigram()
        {
            using (Dialogs.Loading("Iniciando sesión", null, null, true, MaskType.Black))
            {
                var json = await new Models.Service().Login(usuario.Text, contraseña.Text);
                if (json == null)
                    await this.DisplayAlert("Notigram","Usuario o contraseña invalido","Aceptar");
                else
                {
                    Console.WriteLine("Datos del usuario "+json.user_email);
                    Preferences.Set("token", json.token);
                    Preferences.Set("user_email", json.user_email);
                    Preferences.Set("user_display_name", json.user_display_name);
                    Preferences.Set("user_id", json.user_id);
                    Application.Current.MainPage = new View.Menu();
                }
            }
        }
        protected override bool OnBackButtonPressed()
        {
            Application.Current.MainPage = new View.Menu();
            return base.OnBackButtonPressed();
        }

    }
}