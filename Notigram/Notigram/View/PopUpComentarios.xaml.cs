﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PopUpComentarios : Rg.Plugins.Popup.Pages.PopupPage
    {
        #region Variables
        String id, post;
        IUserDialogs Dialogs;
        #endregion
        public PopUpComentarios(string id, string post)
        {
            InitializeComponent();
            Dialogs = UserDialogs.Instance;
            nombre.Text = Preferences.Get("user_display_name", "");
            correo.Text = Preferences.Get("user_email", "");
            this.id = id;
            this.post = post;
        }
        private void onClicked(object sender, EventArgs e)
        {
            ValidarCampos();
        }

        private async void ValidarCampos()
        {
            //var emailPattern = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
            if(String.IsNullOrWhiteSpace(correo.Text) || String.IsNullOrWhiteSpace(comentario.Text) 
                || String.IsNullOrWhiteSpace(nombre.Text))
            {
                await this.DisplayAlert("Advertencia","Todos los campos son obligatorios","Aceptar");
            }
            else
            {
                postComment();
            }
        }

        private async void postComment()
        {
            using(Dialogs.Loading("Publicando", null, null, true, MaskType.Black))
            {
                if(await new Models.Service().PostComentarios(post,correo.Text,nombre.Text,comentario.Text,id))
                {
                    await this.DisplayAlert("Notigram", "Tu comentario tiene que ser aprobado", "Aceptar");
                }
                else
                {
                    await this.DisplayAlert("Notigram", "Algo salio mal al publicar el comentario", "Aceptar");
                }
            }
        }

        private void onClickedCancel(object sender, EventArgs e)
        {
            PopupNavigation.Instance.PopAsync();
        }
    }
}