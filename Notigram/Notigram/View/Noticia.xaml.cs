﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Noticia : ContentPage
    {
        #region Variables
        Control.Noticias noticias { get; set; }
        IUserDialogs Dialogs;
        Control.Comentarios comentarios { get; set; }
        public List<Control.Comentarios> Comentarios { get; set; }
        #endregion
        public Noticia(Control.Noticias noticias)
        {
            InitializeComponent();
            
            this.BindingContext = new CardViewComentarios();
            this.noticias = noticias;
            Title = "Inicio";
            Comentarios = new List<Control.Comentarios>();
            Dialogs = UserDialogs.Instance;
            image.Source = this.noticias.url_image;
            author.Text = this.noticias.nameAuthor;
            category.Text = this.noticias.category;
            date.Text = this.noticias.date;
            title.Text = this.noticias.rendered.Replace("&#8220;", "\"").Replace("&#8221;", "\"");
            nombre.Text = Preferences.Get("user_display_name", "");
            correo.Text = Preferences.Get("user_email", null);
            correo.IsReadOnly = Preferences.ContainsKey("user_email") ? true : false;
            correo.IsReadOnly = Preferences.ContainsKey("user_dislpay_name") ? true : false;
            Console.WriteLine(this.noticias.content.GetValue("rendered").ToString());
            
            lsComentarios.ItemSelected += ItemSelected;
            HtmlWebViewSource w;
            if(Device.RuntimePlatform == Device.iOS)
            {
                web.HeightRequest = 300;
            }
            if (this.noticias.content.GetValue("rendered").ToString().Equals(""))
            {
                w = new HtmlWebViewSource
                {
                    Html = @"<html><body><script async defer crossorigin='anonymous'
                    src='https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0'>
                    </script>
                    <div class='fb-video' data-href='{0}' data-allowfullscreen='true' 
                    data-show-text='false' data-width='540'> </body></html>".Replace("{0}", this.noticias.videoFb)
                };
                web.HeightRequest = 200;
            }
            else
            {
                w = new HtmlWebViewSource
                {
                    Html = "<html><head><meta charset='utf-8'></head><body>" +
                this.noticias.content.GetValue("rendered").ToString().Trim()
                + "</body></html>"
,
                };
            }
            web.Source = w;
            /*if (this.noticias.videoFb!=null)
            {
                wb.IsVisible = true;
                var video = new HtmlWebViewSource
                {
                    Html = @"<script async defer crossorigin='anonymous'
                src='https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0'>
                </script>
                <div class='fb-post' data-href='{0}' data-allowfullscreen='true'
                data-show-text='false' data-width='540'>".Replace("{0}",this.noticias.videoFb)
                };
                wb.Source = video;
            }*/
            web.Reload();
        }

        async void ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            comentarios = lsComentarios.SelectedItem as Control.Comentarios;
            lsComentarios.SelectedItem = null;
            if (lsComentarios.SelectedItem != null) 
            {
                if(!comentarios.comentarios.Equals("0 Comentarios"))
                    await Navigation.PushModalAsync(new View.Comentarios(comentarios), true);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            getComentarios(); 
        }

        async void getComentarios()
        {
            Comentarios.Clear();
            
            DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("es-ES", false).DateTimeFormat;
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black)) 
            {
                
                //var json = await new Models.Service().GetComentarios(this.noticias.id);
                var json = await new Models.Service().GetComentarios("?post="+this.noticias.id);
                int c = 0;
                Console.WriteLine("Contador "+(json.Count-1));
                if (json != null)
                {
                    for (int i = json.Count - 1; i >= 0; i--)
                    {
                        if (json[i].parent == 0)
                        {
                            c++;
                            //Console.WriteLine("Nombre "+json[0].author_name);
                            DateTime date = DateTime.Parse(json[i].date);
                            var jResponse = await new Models.Service().GetComentarios("?parent="+json[i].id);
                            var men = jResponse == null ? 0 : jResponse.Count;
                            var numComents = jResponse == null ? "0 Comentarios" : men==1 ? men+" Comentario" :men+" Comentarios";
                            Comentarios.Add(new Control.Comentarios
                            {
                                author_name = json[i].author_name,
                                date = dateTimeFormatInfo.GetMonthName(date.Month).Substring(0, 3).ToUpper() + "/" + date.Day + "  " + date.TimeOfDay.ToString().Substring(0, 5),
                                icon = json[i].author_avatar_urls.GetValue("96").ToString(),
                                body = json[i].content.GetValue("rendered").ToString().Replace("<p>", "").Replace("</p>", "").Replace("<br />",""),
                                id = json[i].id,
                                comentarios = numComents,
                                post = json[0].post
                            });
                        }
                    }
                    lsComentarios.HeightRequest = (60 * c) + (10 * c);
                    lsComentarios.ItemsSource = Comentarios;
                }
            }
        }

        private async void clickedComment(object sender, EventArgs e)
        {
            if (String.IsNullOrWhiteSpace(nombre.Text) || String.IsNullOrWhiteSpace(correo.Text)
                || String.IsNullOrWhiteSpace(comentario.Text))
            {
                await this.DisplayAlert("Advertencia", "Todos los campos son obligatorios", "Aceptar");
            }
            else
            {
                using (Dialogs.Loading("Publicando", null, null, true, MaskType.Black))
                {
                    if (await new Models.Service().PostComentarios(noticias.id, correo.Text,
                        nombre.Text, comentario.Text, null))
                    {
                        await this.DisplayAlert("Notigram", "Tu comentario tiene que ser aprobado", "Aceptar");
                    }
                    else
                        await this.DisplayAlert("Notigram", "Upsss... Algo salio mal\nIntentelo mas tarde", "Aceptar");
                }
            }
        }
    }
}