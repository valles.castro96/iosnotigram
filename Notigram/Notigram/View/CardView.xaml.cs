﻿using Plugin.Share;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardView : ViewCell
    {

        public CardView()
        {
            InitializeComponent();

            image.CacheDuration = TimeSpan.FromDays(90);
            image.DownsampleToViewSize = true;
            image.RetryCount = 10;
            image.RetryDelay = 2000;
        }

        private void onClicked(object sender, EventArgs e)
        {
            var objecto = ((ImageButton)sender);
            var link = (Control.Noticias)objecto.CommandParameter;
            CrossShare.Current.Share(new Plugin.Share.Abstractions.ShareMessage
            {
                Title = "Notigram",
                Url = link.link,
                Text = link.rendered.ToString()
            });
            //87d438bd06d1c42e741fac5c117a6538
        }

        void Button_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine("Se presiono el boton");
            var objeto = ((ImageButton)sender);
            var noticia = (Control.Noticias)objeto.CommandParameter;
            Console.WriteLine(noticia.link);
            //this.Navigation.PushAsync(new Noticia(noticia), true);
        }

        private async void Facebook_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.Noticias)o.CommandParameter;
            //Console.WriteLine("link "+objeto.user_meta.ToString());
            await Browser.OpenAsync(objeto.user_meta.GetValue("facebook").ToString(), new BrowserLaunchOptions 
            { 
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }

        private async void Twitter_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.Noticias)o.CommandParameter;
            await Browser.OpenAsync("https://twitter.com/"+objeto.user_meta.GetValue("twitter").ToString(), new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }

        private async void Instagram_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.Noticias)o.CommandParameter;
            await Browser.OpenAsync(objeto.user_meta.GetValue("instagram").ToString(), new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }
    }
}