﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class horoscopos : Rg.Plugins.Popup.Pages.PopupPage
    {
        #region Variables
        public List<Control.Menu> signos { get; set; }
        string[] s = new string[] { "Aries", "Tauro", "Geminis","Cancer","Leo","Virgo",
            "libra","Escorpio","Sagitario","Capricornio","Acuario","Piscis"};
        public Control.Obituarios obituarios { get; set; }
        #endregion
        public horoscopos(Control.Obituarios obituarios)
        {
            InitializeComponent();
            fecha.Text = obituarios.rendered;
            imagen.Source = obituarios.url_image;
            signos = new List<Control.Menu>();
            this.obituarios = obituarios;
            lsSignos.ItemSelected += LsSignos_ItemSelected;
        }

        private void LsSignos_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lsSignos.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            JArray a = JArray.Parse(Constant.Constants.content);
            for(int i=0; i < s.Length; i++)
            {
                signos.Add(new Control.Menu
                {
                    name = s[i],
                    image = a[i].ToString(),
                    term_id = obituarios.contenidoHoroscopo.GetValue(s[i].ToLower()).ToString()
                });
            }
            lsSignos.ItemsSource = signos;
        }
    }
}