﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.OpenWhatsApp;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardviewReporteros : ViewCell
    {
        public CardviewReporteros()
        {
            InitializeComponent();
        }

        private async void Facebook_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.AuthorName)o.CommandParameter;
            //Console.WriteLine("link "+objeto.user_meta.ToString());
            await Browser.OpenAsync(objeto.meta.GetValue("facebook").ToString(), new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }

        private async void Twitter_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.AuthorName)o.CommandParameter;
            await Browser.OpenAsync("https://twitter.com/" + objeto.meta.GetValue("twitter").ToString(), new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }

        private async void Instagram_Clicked(object sender, EventArgs e)
        {
            var o = ((ImageButton)sender);
            var objeto = (Control.AuthorName)o.CommandParameter;
            await Browser.OpenAsync(objeto.meta.GetValue("instagram").ToString(), new BrowserLaunchOptions
            {
                LaunchMode = BrowserLaunchMode.SystemPreferred,
                TitleMode = BrowserTitleMode.Show,
                PreferredToolbarColor = Color.FromHex("#042f66"),
                PreferredControlColor = Color.FromHex("#2a98bd")
            });
        }

        private void Whatsapp_Clicked(object sender, EventArgs e)
        {
            try
            {
                var objeto = ((ImageButton)sender);
                var author = (Control.AuthorName)objeto.CommandParameter;
                Chat.Open("+52"+author.meta.GetValue("whatsapp").ToString(), "Hola "+author.name);
            }catch(Exception m)
            {
                Console.WriteLine("eror whatsapp " +m.Message);
                
            }
        }
    }
}