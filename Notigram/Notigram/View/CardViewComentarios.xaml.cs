﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardViewComentarios : ViewCell
    {
        #region variables
        #endregion
        public CardViewComentarios()
        {
            InitializeComponent();            
        }

        private void onClicked(object sender, EventArgs e)
        {
            var objecto = ((Button)sender);
            var comment = (Control.Comentarios)objecto.CommandParameter;
            Console.WriteLine("id del comentario "+comment.id);
            PopupNavigation.Instance.PushAsync(new PopUpComentarios(comment.id,comment.post));
        }
    }
}