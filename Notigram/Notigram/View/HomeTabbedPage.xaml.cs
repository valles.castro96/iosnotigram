﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HomeTabbedPage : TabbedPage
    {
        #region variables
        IUserDialogs Dialogs;
        #endregion
        public HomeTabbedPage() {
            InitializeComponent(); 
            //using()
        }

        public HomeTabbedPage(string id, string title)
        {
            InitializeComponent();
            this.BindingContext = this;
            Title = title;
            
            if (title.Equals("Obituarios") | title.Equals("Horóscopos"))
            {

                Children.Add(new Mas(title) { IconImageSource = ImageSource.FromResource("Notigram.Resource.news_56.png") });
            }
            else
            {
                Children.Add(new MainPage(id, title) { IconImageSource = ImageSource.FromResource("Notigram.Resource.news_56.png") });
            }
                
            Children.Add(new Destacadas() { IconImageSource = ImageSource.FromResource("Notigram.Resource.pin_56.png") });
            Children.Add(new Publicar() { IconImageSource = ImageSource.FromResource("Notigram.Resource.trans.png") });
            Children.Add(new ReporteCiudadano() { IconImageSource = ImageSource.FromResource("Notigram.Resource.chat.png") });
            if (Preferences.ContainsKey("user_email"))
            {
                Children.Add(new Perfil() { IconImageSource = ImageSource.FromResource("Notigram.Resource.setting.png") });
            }
            else
            {
                Children.Add(new Login { IconImageSource = ImageSource.FromResource("Notigram.Resource.setting.png") });
            }
        }

        public void Refresh(string id,string title) {
            if (id != null)
            {
                Children[0] = new MainPage(id, title){ IconImageSource = ImageSource.FromResource("Notigram.Resource.news_56.png") };
                var x = (MainPage)Children[0];
                Title = title;
                x.Refresh(id, title);
            }else
            {
                Children[0] = new Mas(title) { IconImageSource = ImageSource.FromResource("Notigram.Resource.news_56.png") };
                var x =(View.Mas)Children[0];
                Title = title;
                x.Refresh(title);
                
            }
        }
    }
}