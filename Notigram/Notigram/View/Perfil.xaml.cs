﻿using Acr.UserDialogs;
using Android.App;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Perfil : ContentPage
    {
        #region Variables
        TapGestureRecognizer CerrarSesion = new TapGestureRecognizer();
        TapGestureRecognizer Contraseña = new TapGestureRecognizer();
        TapGestureRecognizer Noticias = new TapGestureRecognizer();
        public List<Control.Menu> menus { get; set; }
        Control.Menu Menu { get; set; }
        IUserDialogs Dialog;
        #endregion
        public Perfil()
        {
            InitializeComponent();
            menus = new List<Control.Menu>();
            Dialog = UserDialogs.Instance;
            usuario.Text = Preferences.Get("user_display_name", "");
            correo.Text = Preferences.Get("user_email", "");
            CerrarSesion.Tapped += CerrarSesion_Tapped;
            Contraseña.Tapped += Contraseña_Tapped;
            Noticias.Tapped += Noticias_Tapped;
            close.GestureRecognizers.Add(CerrarSesion);
            forgot.GestureRecognizers.Add(Contraseña);
            Return.GestureRecognizers.Add(Noticias);
            lsCiudades.ItemSelected += LsCiudades_ItemSelected;
        }

        private void LsCiudades_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            lsCiudades.SelectedItem = null;
            /*
            Menu = lsCiudades.SelectedItem as Control.Menu;
            lsCiudades.SelectedItem = null;
            if (lsCiudades.SelectedItem != null)
            {
                if (Menu != null & Preferences.ContainsKey(Menu.name))
                {
                    //Console.WriteLine("Abrir pop up");
                    //await PopupNavigation.Instance.PushAsync(new PopUpSubMenu(Menu));
                    Menu = null;
                    lsCiudades.SelectedItem = null;
                }
            }*/
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetCiudades();
        }

        private async void GetCiudades()
        {
            using (Dialog.Loading("Cargando", null, null, true, MaskType.Black))
            {
                var json = await new Models.Service().GetMenuR();
                var jsonD = JsonConvert.DeserializeObject<List<Control.Menu>>(json.GetValue("Durango").ToString());
                var jsonC = JsonConvert.DeserializeObject<List<Control.Menu>>(json.GetValue("Chihuahua").ToString());
                for(int i = 0; i < jsonD.Count; i++)
                {
                    menus.Add(new Control.Menu() 
                    { 
                        id = jsonD[i].id,
                        name = jsonD[i].name,
                        idInt = int.Parse(jsonD[i].id)
                    });
                }
                for(int i = 0; i < jsonC.Count; i++)
                {
                    menus.Add(new Control.Menu()
                    {
                        id = jsonC[i].id,
                        name = jsonC[i].name
                    });
                }
                lsCiudades.ItemsSource = menus;
            }
        }

        private async void Noticias_Tapped(object sender, EventArgs e)
        {
            await Navigation.PopModalAsync();
        }

        private void Contraseña_Tapped(object sender, EventArgs e)
        {
            
        }

        private void CerrarSesion_Tapped(object sender, EventArgs e)
        {
            Preferences.Clear();
            Xamarin.Forms.Application.Current.MainPage = new View.Menu();
        }

    }
}