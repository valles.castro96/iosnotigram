﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Destacadas : ContentPage
    {
        #region
        IUserDialogs Dialogs;
        Control.Noticias Noticias { get; set; }
        public List<Control.Noticias> noticias { get; set; }
        #endregion
        public Destacadas()
        {
            InitializeComponent();
            this.BindingContext = new CardView();
            noticias = new List<Control.Noticias>();
            Dialogs = UserDialogs.Instance;
            lsNoticias.RefreshCommand = new Command(() =>
            {
                lsNoticias.IsRefreshing = true;
                GetNoticias();
                lsNoticias.IsRefreshing = false;
            });
            lsNoticias.ItemSelected += LsNoticias_ItemSelected;
        }

        private async void LsNoticias_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            this.Noticias = lsNoticias.SelectedItem as Control.Noticias;
            lsNoticias.SelectedItem = null;
            if (lsNoticias.SelectedItem != null)
                await Navigation.PushAsync(new Noticia(this.Noticias), true);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            GetNoticias();
        }

        async void GetNoticias()
        {
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black)) 
            {
                var json = await new Models.Service().GetNoticiasHttp("2,3,4,5&per_page=15","1");
                Console.WriteLine("Dimension "+json.Count);
                for (int i = 0; i < json.Count()-1; i++) 
                {
                    List<Control.Category> category = JsonConvert.DeserializeObject<List<Control.Category>>(json[i]._embedded.GetValue("wp:term")[0].ToString());
                    DateTime date = DateTime.Parse(json[i].date);
                    DateTime hour = DateTime.Now;
                    DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("es-ES", false).DateTimeFormat;
                    var author = JsonConvert.DeserializeObject<Control.AuthorName>(json[i]._embedded.GetValue("author")[0].ToString());
                    var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>(json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                    var tags = JsonConvert.DeserializeObject<List<Control.CiudadesReporte>>(json[i]._embedded.GetValue("wp:term")[1].ToString());
                    string tag = "";
                    int count = tags.Count >= 3 ? 3 : tags.Count;
                    for (int y=0; y < count; y++)
                    {
                        tag = tag + " #" + tags[y].name.Replace(" ", "");
                    }
                    string fecha = "";
                    if ((hour - date).Days == 0 & (hour - date).Hours == 0 & (hour - date).Minutes >= 0)
                    {
                        fecha = (hour - date).Minutes == 1 || (hour - date).Minutes == 0 ? "Ahora" : (hour - date).Minutes + " minutos";
                    }
                    else if ((hour - date).Days == 0 & (hour - date).Hours >= 0 & (hour - date).Minutes >= 0)
                    {
                        fecha = (hour - date).Hours == 1 ? (hour - date).Hours + " hora" : (hour - date).Hours + " horas";
                    }
                    else if ((hour - date).Days >= 1 & (hour - date).Days < 2)
                    {
                        fecha = "Ayer";
                    }
                    else if ((hour - date).Days >= 2)
                    {
                        fecha = date.Month + "/" + date.Day + "/" + date.Year;
                    }
                    try
                    {
                        string url;
                        var full = JsonConvert.DeserializeObject<Control.Image>(media_details.media_details.GetValue("sizes").ToString());
                        try
                        {
                            url = AsignUrl(full.medium_large.GetValue("source_url").ToString());
                        }
                        catch (Exception e)
                        {
                            url = AsignUrl(full.medium.GetValue("source_url").ToString());
                        }
                        //Console.WriteLine("Contador"+i);
                        noticias.Add(new Control.Noticias
                        {
                            url_image = ImageSource.FromUri(new Uri(url)),
                            category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                            tag = tag,
                            //slug = "@" + author.slug,
                            url_avatar = author.avatar_urls.GetValue("96").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                            //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                            date = fecha,
                            link = json[0].link,
                            id = json[0].id,
                            nameAuthor = author.name,
                            content = json[i].content,
                            noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                            user_meta = json[i].user_meta,
                            videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                            facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                        });
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e.Message);
                        noticias.Add(new Control.Noticias
                        {
                            url_image = ImageSource.FromResource("Notigram.Resource.notigram_no_image.jpg", typeof(Constant.Images).GetTypeInfo().Assembly),
                            category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                            tag = tag,
                            //slug = "@" + author.slug,
                            url_avatar = author.avatar_urls.GetValue("96").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                            //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                            date = fecha,
                            link = json[0].link,
                            id = json[0].id,
                            nameAuthor = author.name,
                            content = json[i].content,
                            noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                            user_meta = json[i].user_meta,
                            videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                            facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                        });
                    }

                }
                lsNoticias.ItemsSource = noticias;
            }
        }

        private string AsignUrl(string url)
        {
            //Console.WriteLine(url.Substring(0, (url.Length - 5 )));
            if (url.EndsWith(".webp"))
            {
                return url.Substring(0, (url.Length - 5));
            }
            else
                return url;
            //Console.WriteLine(url.Replace(".webp", ""));
        }
    }
}