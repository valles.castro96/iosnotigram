﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MenuD : Rg.Plugins.Popup.Pages.PopupPage
    {
        #region Variables
        public List<Control.Menu> MenuC { get; set; }
        public List<Control.Menu> MenuDur { get; set; }
        Control.Menu menu { get; set; }
        TapGestureRecognizer tapGesture = new TapGestureRecognizer();
        #endregion
        public MenuD()
        {
            InitializeComponent();
            MenuC = new List<Control.Menu>();
            MenuDur = new List<Control.Menu>();
            BindingContext = this;
            tapGesture.Tapped += TapGesture_Tapped;
            notigram.GestureRecognizers.Add(tapGesture);
            Principal.WidthRequest = Application.Current.MainPage.Width * 0.75f;
        }

        private void TapGesture_Tapped(object sender, EventArgs e)
        {
            if (Device.RuntimePlatform == Device.iOS)
            {
                var home = ((Menu)Application.Current.MainPage);

                home.Refresh("0", "Inicio");

            }
            else if (Device.RuntimePlatform == Device.Android)
            {
                Application.Current.MainPage = new View.Menu();
            }
            PopupNavigation.Instance.PopAsync();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            lsChihuahua.WidthRequest = (DeviceDisplay.MainDisplayInfo.Width / 5);
            lsDurango.WidthRequest = (DeviceDisplay.MainDisplayInfo.Width / 5);
            lsChihuahua.ItemSelected += LsChihuahua_ItemSelected;
            lsDurango.ItemSelected += LsDurango_ItemSelected;
            GetMenuCities();
        }

        private void LsDurango_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            menu = lsDurango.SelectedItem as Control.Menu;
            lsDurango.SelectedItem = null;
            if(lsDurango.SelectedItem != null)
            {
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);

                    home.Refresh(menu.id, menu.name);

                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    Application.Current.MainPage = new View.Menu(menu.id, menu.name);
                }
                PopupNavigation.Instance.PopAsync();
            }
        }

        private void LsChihuahua_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            menu = lsChihuahua.SelectedItem as Control.Menu;
            lsChihuahua.SelectedItem = null;
            if (lsChihuahua.SelectedItem != null)
            {
                //
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);

                    home.Refresh(menu.id, menu.name);

                }
                else if (Device.RuntimePlatform == Device.Android) {
                    Application.Current.MainPage = new View.Menu(menu.id, menu.name);
                }
                PopupNavigation.Instance.PopAsync();
            }
        }

        private async void GetMenuCities()
        {
            var json = await new Models.Service().GetMenuR();
            var jsonD = JsonConvert.DeserializeObject<List<Control.Menu>>(json.GetValue("Durango").ToString());
            var jsonC = JsonConvert.DeserializeObject<List<Control.Menu>>(json.GetValue("Chihuahua").ToString());

            for(int i=0; i < jsonC.Count(); i++)
            {
                MenuC.Add(new Control.Menu()
                {
                    id = jsonC[i].id,
                    name = jsonC[i].name
                });
            }
            for(int i=0; i<jsonD.Count(); i++)
            {
                MenuDur.Add(new Control.Menu()
                {
                    id = jsonD[i].id,
                    name = jsonD[i].name
                });
            }
            lsChihuahua.ItemsSource = MenuC;
            lsDurango.ItemsSource = MenuDur;
        }
    }
}