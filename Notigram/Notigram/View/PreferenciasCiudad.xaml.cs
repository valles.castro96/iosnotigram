﻿using Acr.UserDialogs;
using Android.App;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PreferenciasCiudad : ViewCell
    {
        #region Variables
        public List<Control.Menu> menus { get; set; }
        IUserDialogs Dialogs;
        #endregion
        public PreferenciasCiudad()
        {
            InitializeComponent();
            Dialogs = UserDialogs.Instance;
            menus = new List<Control.Menu>();
            //Console.WriteLine("Nombre "+check.Text);
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            Console.WriteLine("OnAppearing " + check.Text);
            check.IsChecked = Preferences.ContainsKey(check.Text);
        }


        private void CheckBox_CheckChanged(object sender, EventArgs e)
        {
            Console.WriteLine("Text " + check.Text);
            Console.WriteLine("Key " + check.Key);
            var objeto = (Plugin.InputKit.Shared.Controls.CheckBox)sender;
            var ciudad = ((Control.Menu)objeto.CommandParameter);
            if (Preferences.ContainsKey(ciudad.name))
            {
                Preferences.Remove(ciudad.name);
            }
            else
            {
                Preferences.Set(check.Text, ciudad.id);
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((View.Menu)Xamarin.Forms.Application.Current.MainPage);
                    home.Refresh(ciudad.id, ciudad.name);
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    Xamarin.Forms.Application.Current.MainPage = new View.Menu(ciudad.id, ciudad.name);
                }
                //await PopupNavigation.Instance.PushAsync(new PopUpSubMenu(ciudad));
                //GetSubs();
            }
            //Console.WriteLine("ID ciudad " + ciudad.id);
            //Preferences.Set("ciudades", Preferences.Get("ciudades","")+ciudad.id+"\n");
        }
    }
}