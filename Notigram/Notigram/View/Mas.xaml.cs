﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [DesignTimeVisible(false)]
    public partial class Mas : ContentPage
    {
        #region Variables
        ToolbarItem item;
        Control.Obituarios Obituarios { get; set; }
        ViewModels.MasViewModel masViewModel;
        int elec=1;
        #endregion
        public Mas(String t)
        {
            BindingContext = masViewModel = new ViewModels.MasViewModel();
            InitializeComponent();
            elec = t.Equals("Horóscopos") ? 0 : 1;
            masViewModel.GetContenido(elec);
            lsObituario.ItemAppearing += LsObituario_ItemAppearing;
        }

        public void Refresh(string t)
        {
            BindingContext = masViewModel = new ViewModels.MasViewModel();
            elec = t.Equals("Horóscopos") ? 0 : 1;
            masViewModel.GetContenido(elec);
            lsObituario.ItemAppearing += LsObituario_ItemAppearing;
        }

        private async void LsObituario_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            if(e.ItemIndex == (masViewModel.Obituarios.Count - 1))
            {
                masViewModel.page++;
                await masViewModel.GetMasContenido(elec);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            masViewModel.GetContenido(elec);
            lsObituario.ItemSelected += LsObituario_ItemSelected;
            
        }

        private async void LsObituario_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            Console.WriteLine("Se abrira horoscopo");
            this.Obituarios = lsObituario.SelectedItem as Control.Obituarios;
            lsObituario.SelectedItem = null;
            if(lsObituario.SelectedItem != null)
            {
                if (elec != 1)
                {
                    //await this.DisplayAlert("", "Se abrira oroscopos", elec.ToString());
                    await PopupNavigation.Instance.PushAsync(new horoscopos(this.Obituarios), true);
                }
            }
        }
    }
}