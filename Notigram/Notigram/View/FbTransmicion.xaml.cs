﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FbTransmicion : ContentPage
    {
        public FbTransmicion(Control.Noticias noticias)
        {
            InitializeComponent();
            var w = new HtmlWebViewSource
            {
                Html= @"<html><body><script async defer crossorigin='anonymous'
                    src='https://connect.facebook.net/es_LA/sdk.js#xfbml=1&version=v7.0'>
                    </script>
                    <div class='fb-video' data-href='{0}' data-allowfullscreen='true' 
                    data-show-text='false' data-width='540'> </body></html>".Replace("{0}", noticias.videoFb)
            };
            wb.Source = w;
        }
    }
}