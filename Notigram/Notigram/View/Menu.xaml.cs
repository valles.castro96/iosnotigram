﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Notigram.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Rg.Plugins.Popup.Services;
using Newtonsoft.Json.Linq;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    //IconImageSource="{Binding PlatformSpecificImage}"
    public partial class Menu : MasterDetailPage
    {
        #region Variables
        public List<Control.Menu> menus { get; set; }
        public List<Control.Menu> menusC { get; set; }
        Control.Menu itemSelected { get; set; }
        IUserDialogs Dialogs;
        string idCity, city;
        #endregion
        public Menu()
        {
            InitializeComponent();

            //Detail = new NavigationPage(new HomeTabbedPage("0", "Inicio"));
            this.idCity = null;
            Detail = new NavigationPage(new HomeTabbedPage("0","Inicio"));
            Dialogs = UserDialogs.Instance;
            menus = new List<Control.Menu>();
            menusC = new List<Control.Menu>();
            BindingContext = this;
            lsMenuR.ItemSelected += Ls_ItemSelected;
            subMenu.ItemSelected += SubMenu_ItemSelected;
        }

        

        public Menu(string idCity, string city)
        {
            InitializeComponent();
            this.idCity = idCity;
            this.city = city;
            Dialogs = UserDialogs.Instance;
            menus = new List<Control.Menu>();
            menusC = new List<Control.Menu>();
            BindingContext = this;
            lsMenuR.ItemSelected += Ls_ItemSelected;
            subMenu.ItemSelected += SubMenu_ItemSelected;
        }

        public void Refresh(string idCity, string city) {
            this.idCity = idCity;
            this.city = city;
            Dialogs = UserDialogs.Instance;
            menus = new List<Control.Menu>();
            menusC = new List<Control.Menu>();
            BindingContext = this;
            lsMenuR.ItemSelected += Ls_ItemSelected;

            if (idCity.Equals("0"))
            {
                subMenu.IsVisible = false;
                getMenu();
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);
                    var x = home.Detail;
                    var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                    y.Refresh("0", "Inicio");
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    Detail = new NavigationPage(new HomeTabbedPage());
                }
            }
            else {
                GetSubMenu();
            }
            
        }

        async void getMenu()
        {
            if(!await new Service().ValidateToken() && Preferences.ContainsKey("token"))
            {
                await new Service().Login(Preferences.Get("email", ""), 
                    Preferences.Get("password", ""));
            }
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black)) {
                var json = await new Service().GetMenu();
                for (int i = 0; i < json.Count; i++)
                {
                    string url = json[i].image;
                    menus.Add(new Control.Menu()
                    {
                        term_id = json[i].term_id,
                        name = json[i].name,
                        image = json[i].image == null ? "" : url.Substring(0, (url.Length - 5))
                    });
                }
                JArray a = JArray.Parse(Constant.Constants.imgs);
                for(int i = 0; i < Constant.Constants.otros.Length; i++)
                {
                    menus.Add(new Control.Menu()
                    {
                        name = Constant.Constants.otros[i],
                        image = a[i].ToString()
                    });
                }

                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);
                    var x = home.Detail;
                    var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                    y.Refresh("0","Inicio");
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    //Detail = new NavigationPage(new HomeTabbedPage("0", "Inicio"));
                }

                lsMenuR.ItemsSource = menus;
            }
        }

        private async void GetSubMenu()
        {
            string ids = idCity + ",";
            if (!await new Service().ValidateToken() && Preferences.ContainsKey("token"))
            {
                await new Service().Login(Preferences.Get("email", ""),
                    Preferences.Get("password", ""));
            }
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
            {
                var jsonP = await new Service().GetMenu();
                for (int i = 0; i < jsonP.Count; i++)
                {
                    string url = jsonP[i].image;
                    menus.Add(new Control.Menu()
                    {
                        term_id = jsonP[i].term_id,
                        name = jsonP[i].name,
                        image = jsonP[i].image == null ? "" : url.Substring(0, (url.Length - 5))
                    });
                }
                JArray a = JArray.Parse(Constant.Constants.imgs);
                for (int i = 0; i < Constant.Constants.otros.Length; i++)
                {
                    menus.Add(new Control.Menu()
                    {
                        name = Constant.Constants.otros[i],
                        image = a[i].ToString()
                    });
                }
                lsMenuR.HeightRequest = menus.Count * 100 / 2.5;
                lsMenuR.ItemsSource = menus;

                var json = await new Service().GetCityMenu(idCity);
                lugar.Text = city;
                subMenu.IsVisible = true;
                int f = 0;
                for (int i = 0; i < json.Count; i++)
                {
                    ids += json[i].id + ",";
                    if (Preferences.ContainsKey(json[i].name))
                    {
                        
                        menusC.Clear();
                        menusC.Add(new Control.Menu()
                        {
                            id = json[i].id,
                            name = json[i].name,
                            image = json[i].image == null ? "" : json[i].image
                        });
                        f++;
                    }else if(!Preferences.ContainsKey(json[i].name) & f==0)
                    {
                        menusC.Add(new Control.Menu()
                        {
                            id = json[i].id,
                            name = json[i].name,
                            image = json[i].image == null ? "" : json[i].image
                        });
                        
                    }
                }

                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);
                    var x = home.Detail;
                    var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                    y.Refresh(ids, city);
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    Detail = new NavigationPage(new HomeTabbedPage(ids, city));
                }
                subMenu.HeightRequest = menusC.Count * 100 / 2.5;
                subMenu.HasUnevenRows = false;
                subMenu.ItemsSource = menusC;
            }
        }

        private void SubMenu_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            itemSelected = subMenu.SelectedItem as Control.Menu;
            if(itemSelected != null)
            {
                IsPresented = false;
                if (Device.RuntimePlatform == Device.iOS)
                {
                    var home = ((Menu)Application.Current.MainPage);
                    var x = home.Detail;
                    var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                    y.Refresh(itemSelected.id, itemSelected.name);
                }

                if (Device.RuntimePlatform == Device.Android)
                {
                    Detail = new NavigationPage(new HomeTabbedPage(itemSelected.id, itemSelected.name));
                }
                subMenu.SelectedItem = null;
            }
        }

        private void Ls_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            itemSelected = lsMenuR.SelectedItem as Control.Menu;
            if (itemSelected != null)
            {
                IsPresented = false;
                if (itemSelected.term_id != null)
                {
                    Title = itemSelected.name;
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        var home = ((Menu)Application.Current.MainPage);
                        var x = home.Detail;
                        var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                        y.Refresh(itemSelected.term_id, itemSelected.name);
                    }

                    if (Device.RuntimePlatform == Device.Android)
                    {
                        Detail = new NavigationPage(new HomeTabbedPage(itemSelected.term_id, itemSelected.name));
                    }

                }
                else
                {
                    if (Device.RuntimePlatform == Device.iOS)
                    {
                        var home = ((Menu)Application.Current.MainPage);
                        var x = home.Detail;
                        var y = (HomeTabbedPage)x.Navigation.NavigationStack[0];
                        y.Refresh(itemSelected.id, itemSelected.name);
                    }

                    if (Device.RuntimePlatform == Device.Android)
                    {
                        Detail = new NavigationPage(new HomeTabbedPage(itemSelected.id, itemSelected.name));
                    }

                }
                lsMenuR.SelectedItem = null;
            }
        }

        async void Espera() {
            await Task.Delay(100);
            //await Navigation.PopAsync();
        }

        private async void ToolbarItem_Clicked(object sender, EventArgs e)
        {
            await PopupNavigation.Instance.PushAsync(new MenuD());
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            if (idCity != null)
            {
                GetSubMenu();
            }
            else
            {
                getMenu();
            }
        }
    }
}