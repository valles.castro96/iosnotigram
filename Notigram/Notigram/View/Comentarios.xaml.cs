﻿using Acr.UserDialogs;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Comentarios : ContentPage
    {
        #region variables
        IUserDialogs Dialogs;
        Control.Comentarios comments { get; set; }
        Control.Comentarios comment { get; set; }
        public List<Control.Comentarios> GetComentarios { get; set; }
        #endregion
        public Comentarios(Control.Comentarios comentarios)
        {
            InitializeComponent();
            GetComentarios = new List<Control.Comentarios>();
            Dialogs = UserDialogs.Instance;
            this.comments = comentarios;
            this.BindingContext = this;
            this.comentarios.Text = comments.comentarios.ToString();
            icon.Source = comments.icon;
            author_name.Text = comments.author_name;
            this.date.Text = comments.date;
            body.Text = comments.body;
            lsComentarios.ItemSelected += LsComentarios_ItemSelected;
        }

        private async void LsComentarios_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            comment = lsComentarios.SelectedItem as Control.Comentarios;
            lsComentarios.SelectedItem = null;
            if(lsComentarios.SelectedItem != null)
            {
                if (!comment.comentarios.Equals("0 Comentarios"))
                    await Navigation.PushAsync(new View.Comentarios(comment), true);
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            getCommentarios();
        }

        async void getCommentarios()
        {
            DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("es-ES", false).DateTimeFormat;
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
            {
                var json = await new Models.Service().GetComentarios("?parent=" + this.comments.id);
                for (int i = json.Count - 1; i >= 0; i--)
                {
                    DateTime date = DateTime.Parse(json[i].date);
                    var jResponse = await new Models.Service().GetComentarios("?parent=" + json[i].id);
                    var men = jResponse == null ? 0 : jResponse.Count;
                    var numComents = jResponse == null ? "0 Comentarios" : men == 1 ? men + " Comentario" : men + " Comentarios";
                    GetComentarios.Add(new Control.Comentarios 
                    {
                        author_name = json[i].author_name,
                        date = dateTimeFormatInfo.GetMonthName(date.Month).Substring(0, 3).ToUpper() + "/" + date.Day + "  " + date.TimeOfDay.ToString().Substring(0, 5),
                        icon = json[i].author_avatar_urls.GetValue("96").ToString(),
                        body = json[i].content.GetValue("rendered").ToString().Replace("<p>", "").Replace("</p>", "").Replace("<br />",""),
                        id = json[i].id,
                        comentarios = numComents
                    });
                }
                lsComentarios.ItemsSource = GetComentarios;
            }
        }

        private async void ButtomRender_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine("id del comentario "+comments.author_name);
            await PopupNavigation.Instance.PushAsync(new PopUpComentarios(comments.id, comments.post));
        }
    }
}