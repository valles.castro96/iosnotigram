﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Notigram.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SignUp : ContentPage
    {
        #region Variables
        string emailPattern = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
        string passRegex = "^.{8,50}$";
        string nameRegex = "^([a-zA-Z])*$";
        IUserDialogs Dialogs;
        TapGestureRecognizer recognizer = new TapGestureRecognizer();
        #endregion
        public SignUp()
        {
            InitializeComponent();
            Dialogs = UserDialogs.Instance;
            recognizer.Tapped += Recognizer_Tapped;
            login.GestureRecognizers.Add(recognizer);
        }

        private async void Recognizer_Tapped(object sender, EventArgs e)
        {
            await this.Navigation.PopModalAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            this.Navigation.PopModalAsync();
            return base.OnBackButtonPressed();
        }

        private bool ValidateEntrys()
        {
           if(String.IsNullOrEmpty(nombre.Text) || String.IsNullOrEmpty(usuario.Text)
                || String.IsNullOrEmpty(contraseña.Text))
            {
                this.DisplayAlert("Notigram","Los campos no pueden estar vacios","Aceptar");
                return false;
            }
            else
            {
                if (!Regex.IsMatch(nombre.Text, nameRegex))
                {
                    this.DisplayAlert("Notigram", "El nombre no puede tener espacio ni numeros", "Aceptar");
                    return false;
                }
                else
                {
                    if (!Regex.IsMatch(usuario.Text, emailPattern))
                    {
                        this.DisplayAlert("Notigram", "Debe de ingresar un correo valido", "Aceptar");
                        return false;
                    }
                    else
                    {
                        if (!Regex.IsMatch(contraseña.Text, passRegex))
                        {
                            this.DisplayAlert("Notigram", "La contraseña debe de ser al menos de 8 caracteres", "Aceptar");
                            return false;
                        }
                    }
                }
                return true;
            }
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            CreateUser(ValidateEntrys());
        }

        private async void CreateUser(bool v)
        {
            if (v)
            {
                using(Dialogs.Loading("Creando usuario",null, null, true, MaskType.Black))
                {
                    if(await new Models.Service().CreateUser(nombre.Text, usuario.Text, contraseña.Text))
                    {
                        await this.DisplayAlert("Notigram", "Se creo el usuario", "Aceptar");
                        await Navigation.PopModalAsync();
                    }
                    else
                    {
                        await this.DisplayAlert("Notigram", "Algo salio mal", "Aceptar");
                    }
                }
            }
        }
    }
}