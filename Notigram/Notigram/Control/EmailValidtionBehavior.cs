﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using Xamarin.Forms;

namespace Notigram.Control
{
    class EmailValidtionBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += BindableOnTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= BindableOnTextChanged;
        }

        private void BindableOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var email = e.NewTextValue;
            var emailPattern = "^([\\w-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|(([\\w-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$";
            var correo = sender as Entry;
            if (Regex.IsMatch(email, emailPattern))
            {
                correo.BackgroundColor = Color.Transparent;
            }
            else
            {
                correo.BackgroundColor = Color.FromHex("ff5131");
            }
        }
    }
    
    class PasswordValidationBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += BindableOnTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= BindableOnTextChanged;
        }
        private void BindableOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var password = e.NewTextValue;
            var passRegex = "^.{8,50}$";
            var pass = sender as Entry;
            if (Regex.IsMatch(password, passRegex))
            {
                pass.BackgroundColor = Color.Transparent;
            }
            else
            {
                pass.BackgroundColor = Color.FromHex("ff5131");
            }
        }
    }

    class NameValidationBehavior : Behavior<Entry>
    {
        protected override void OnAttachedTo(Entry bindable)
        {
            base.OnAttachedTo(bindable);
            bindable.TextChanged += BindableOnTextChanged;
        }

        protected override void OnDetachingFrom(Entry bindable)
        {
            base.OnDetachingFrom(bindable);
            bindable.TextChanged -= BindableOnTextChanged;
        }
        private void BindableOnTextChanged(object sender, TextChangedEventArgs e)
        {
            var nameText = e.NewTextValue;
            var nameRegex = "^([a-zA-Z])*$";
            var name = sender as Entry;
            if (Regex.IsMatch(nameText, nameRegex))
            {
                name.BackgroundColor = Color.Transparent;
            }
            else
            {
                name.BackgroundColor = Color.FromHex("ff5131");
            }
        }
    }
}
