﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace Notigram.Control
{
    public class Horoscopos
    {
        public JObject title { get; set; }
        public JArray astrologos { get; set; }
        public JObject contenidoHoroscopo { get; set; }
        public JObject _embedded { get; set; }
    }
    public class Obituarios
    {
        public string date { get; set; }
        public JObject title { get; set; }
        public string ciudad { get; set; }
        public string funerales { get; set; }
        public JArray ciudades { get; set; }
        public JArray Funeraria { get; set; }
        public JObject contenido { get; set; }
        public JObject _embedded { get; set; }
        public ImageSource url_image { get; set; }
        public string rendered { get; set; }
        public string edad { get; set; }
        public string velacion { get; set; }
        public JObject contenidoHoroscopo { get; set; }
    }

    public class GetMenuCities
    {
        public JObject Durango { get; set; }
        public JObject Chihuahua { get; set; }
    }

    public class CrearReporte
    {
        public string id { get; set; }
    }
    public class Usuario
    {
        public string token { get; set; }
        public string user_email { get; set; }
        public string user_display_name { get; set; }
        public string user_id { get; set; }
    }
    public class TipoReporte
    {
        public string id { get; set; }
        public string name { get; set; }
    }
    public class CiudadesReporte
    {
        public string id { get; set; }
        public string name { get; set; }
    }

    public class Comentarios
    { 
        public string id { get; set; }
        public string post { get; set; }
        public string author_name { get; set; }
        public string date { get; set; }
        public JObject content { get; set; }
        public string rendered { get; set; }
        public JObject author_avatar_urls { get; set; }
        public ImageSource icon { get; set; }
        public string body { get; set; }
        public int parent { get; set; }
        public string comentarios { get; set; }
    }

    public class Menu
    {
        public string term_id { get; set; }
        public string name { get; set; }
        public string image { get; set; }
        public string id { get; set; }
        public int idInt { get; set; }
        public bool IsChecked { get; set; }
    }

    public class Noticias
    {
        public string id { get; set; }
        public string tag { get; set; }
        public string date { get; set; }
        public string link { get; set; }
        public JObject title { get; set; }
        public JObject content { get; set; }
        public JObject _embedded { get; set; }
        public ImageSource url_image { get; set; }
        public ImageSource url_avatar { get; set; }
        public string slug { get; set; }
        public string category { get; set; }
        public string rendered { get; set; }
        public string nameAuthor { get; set; }
        public JObject user_meta { get; set; }
        public string noticieros { get; set; }
        public ImageSource facebook { get; set; }
        public ImageSource twitter { get; set; }
        public ImageSource instagram { get; set; }
        public JArray video_yt { get; set; }
        public JArray video_fb { get; set; }
        public string videoFb { get; set; }
        public string videoYt { get; set; }
    }

    public class FeaturedMedia
    {
        public JObject media_details { get; set; }
    }

    public class Image
    {
        public JObject full { get; set; }
        public JObject thumbnail { get; set; }
        public JObject medium_large { get; set; }
        public JObject medium { get; set; }
    }

    public class Category
    {
        public string name { get; set; }
    }

    public class AuthorName
    {
        public string id { get; set; }
        public string name { get; set; }
        public JObject avatar_urls { get; set; }
        public ImageSource url_image { get; set; }
        public string slug { get; set; }
        public JObject meta { get; set; }
        public string noticiero { get; set; }
        public ImageSource facebook { get; set; }
        public ImageSource twitter { get; set; }
        public ImageSource instagram { get; set; }
        public ImageSource whatsapp { get; set; }
        public string simple_local_avatar { get; set; }
    }
}
