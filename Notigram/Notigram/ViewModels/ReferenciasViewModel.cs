﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Notigram.ViewModels
{
    public class ReferenciasViewModel
    {
        #region Variables
        IUserDialogs Dialogs;
        #endregion
        #region Eventos
        public ObservableCollection<Control.Menu> menus { get; set; }
        #endregion
        public ReferenciasViewModel()
        {
            Dialogs = UserDialogs.Instance;
        }
    }
}
