﻿using Acr.UserDialogs;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Reflection;
using System.Text;
using Xamarin.Forms;

namespace Notigram.ViewModels
{
    class ReporterosViewModel : INotifyPropertyChanged
    {
        #region Eventos
        bool isBusy = false;
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Variables
        public ImageSource PlatformSpecificImage { get; set; }
        ObservableCollection<Control.AuthorName> reporteros { get; set; }
        public ObservableCollection<Control.AuthorName> Reporteros
        {
            get { return reporteros; }
            set
            {
                reporteros = value;
                NotifyPropertyChanged("Reporteros");
            }
        }
        IUserDialogs Dialogs;
        public int page = 1;
        #endregion

        public ReporterosViewModel()
        {
            Dialogs = UserDialogs.Instance;
        }

        public async void GetReportero()
        {
            if (isBusy)
            {
                return;
            }
            isBusy = true;
            //4812,4813
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
            {
                ObservableCollection<Control.AuthorName> authorNames = new ObservableCollection<Control.AuthorName>();

                var json = await new Models.Service().GetReporteros(page);
                for(int i = 0; i < json.Count; i++)
                {
                    authorNames.Add(new Control.AuthorName()
                    {
                        url_image = json[i].simple_local_avatar != null ? 
                                    json[i].simple_local_avatar : json[i].avatar_urls.GetValue("96").ToString(),
                        name = json[i].name,
                        noticiero = json[i].meta.GetValue("noticiero").ToString(),
                        meta = json[i].meta,
                        facebook = json[i].meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        twitter = json[i].meta.GetValue("twitter").ToString() == ""
                                  ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        instagram = json[i].meta.GetValue("instagram").ToString() == ""
                                    ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        whatsapp = json[i].meta.GetValue("whatsapp").ToString() == ""
                                   ? null : ImageSource.FromResource("Notigram.Resource.whatsapp.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                    }) ;
                }
                Reporteros = authorNames;
            }
        }

        public async void GetNewReportero()
        {
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
            {
                var json = await new Models.Service().GetReporteros(page);
                for (int i = 0; i < json.Count; i++)
                {
                    Reporteros.Add(new Control.AuthorName()
                    {
                        url_image = json[i].simple_local_avatar != null ?
                                    json[i].simple_local_avatar : json[i].avatar_urls.GetValue("96").ToString(),
                        name = json[i].name,
                        noticiero = json[i].meta.GetValue("noticiero").ToString(),
                        meta = json[i].meta,
                        facebook = json[i].meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        twitter = json[i].meta.GetValue("twitter").ToString() == ""
                                  ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        instagram = json[i].meta.GetValue("instagram").ToString() == ""
                                    ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                        whatsapp = json[i].meta.GetValue("whatsapp").ToString() == ""
                                   ? null : ImageSource.FromResource("Notigram.Resource.whatsapp.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                    });
                }
            }
        }
    }
}
