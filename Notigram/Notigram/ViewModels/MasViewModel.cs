﻿using Acr.UserDialogs;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;

namespace Notigram.ViewModels
{
    class MasViewModel : INotifyPropertyChanged
    {
        #region Eventos
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
        #region Variables
        ObservableCollection<Control.Obituarios> obituarios { get; set; }
        public ObservableCollection<Control.Obituarios> Obituarios
        {
            get { return obituarios; }
            set
            {
                obituarios = value;
                NotifyPropertyChanged("Obituarios");
            }
        }
        IUserDialogs Dialogs;
        private string o;
        public int page = 1;
        #endregion

        public MasViewModel()
        {
            Dialogs = UserDialogs.Instance;
        }

        public async Task GetMasContenido(int o) {
            if (o == 1)
            {
                using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
                {
                    var json = await new Models.Service().GetObituarios(page);
                    //var c = new Models.Service().GetCiudadObituario(json[0].ciudades[0].ToString());
                    for (int i = 0; i < json.Count; i++)
                    {
                        var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>
                            (json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                        var full = JsonConvert.DeserializeObject<Control.Image>
                            (media_details.media_details.GetValue("sizes").ToString());
                        Obituarios.Add(new Control.Obituarios
                        {
                            url_image = full.thumbnail.GetValue("source_url").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString(),
                            edad = "Fallecío a la edad de " + json[i].contenido.GetValue("edad").ToString(),
                            velacion = json[i].contenido.GetValue("velacion").ToString() + "\n" +
                                       json[i].contenido.GetValue("honras").ToString() + "\n" +
                                       json[i].contenido.GetValue("sepelio").ToString(),
                            ciudad = json[i].ciudades[0].ToString().Equals("6852") ? "Durango" : "Jiménez",
                            funerales = json[i].Funeraria[0].ToString().Equals("6849") ? "Funerales Hernández" : "Funeraria Monte Albán"
                        });
                    }
                }
            }
            else
            {
                using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
                {
                    var json = await new Models.Service().GetHoroscopos(page);
                    for (int i = 0; i < json.Count; i++)
                    {
                        var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>
                            (json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                        var full = JsonConvert.DeserializeObject<Control.Image>
                            (media_details.media_details.GetValue("sizes").ToString());
                        Console.WriteLine(json[i].contenidoHoroscopo.ToString());
                        Obituarios.Add(new Control.Obituarios
                        {
                            url_image = full.thumbnail.GetValue("source_url").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString(),
                            edad = "Paticheman",
                            contenidoHoroscopo = json[i].contenidoHoroscopo
                        });
                    }
                }
            }
        }

        public async void GetContenido(int o)
        {
            page = 1;
            Dialogs = UserDialogs.Instance;
            ObservableCollection<Control.Obituarios> ob = new ObservableCollection<Control.Obituarios>();
            if (o==1)
            {
                using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
                {
                    
                    var json = await new Models.Service().GetObituarios(page);
                    //var c = new Models.Service().GetCiudadObituario(json[0].ciudades[0].ToString());
                    for (int i = 0; i < json.Count; i++)
                    {
                        var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>
                            (json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                        var full = JsonConvert.DeserializeObject<Control.Image>
                            (media_details.media_details.GetValue("sizes").ToString());
                        ob.Add(new Control.Obituarios
                        {
                            url_image = full.thumbnail.GetValue("source_url").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString(),
                            edad = "Fallecío a la edad de " + json[i].contenido.GetValue("edad").ToString(),
                            velacion = json[i].contenido.GetValue("velacion").ToString() + "\n" +
                                       json[i].contenido.GetValue("honras").ToString() + "\n" +
                                       json[i].contenido.GetValue("sepelio").ToString(),
                            ciudad = json[i].ciudades[0].ToString().Equals("6852") ? "Durango" : "Jiménez",
                            funerales = json[i].Funeraria[0].ToString().Equals("6849") ? "Funerales Hernández" : "Funeraria Monte Albán"
                        });
                    }
                    Obituarios = ob;
                }
            }
            else
            {
                using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
                {
                    
                    var json = await new Models.Service().GetHoroscopos(1);
                    for (int i = 0; i < json.Count; i++)
                    {
                        var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>
                            (json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                        var full = JsonConvert.DeserializeObject<Control.Image>
                            (media_details.media_details.GetValue("sizes").ToString());
                        JObject c = new JObject();
                        try
                        {
                            c = json[i].contenidoHoroscopo;
                        }catch (Exception e)
                        {
                            Console.WriteLine("error contenido " + e);
                            c = new JObject();
                        }
                        ob.Add(new Control.Obituarios
                        {
                            url_image = full.thumbnail.GetValue("source_url").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString(),
                            edad = "Paticheman",
                            contenidoHoroscopo = c
                        });
                    }
                    Obituarios = ob;
                }
            }
        }
    }
}
