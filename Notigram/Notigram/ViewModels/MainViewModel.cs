﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.Reflection;
using System.Threading.Tasks;
using Acr.UserDialogs;
using Newtonsoft.Json;
using Xamarin.Forms;

namespace Notigram.ViewModels
{
    public class MainViewModel : INotifyPropertyChanged
    {
        #region eventos
        bool isBusy = false;
        public event PropertyChangedEventHandler PropertyChanged;
        protected virtual void NotifyPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        #region variables
            public ImageSource PlatformSpecificImage { get; set; }
            ObservableCollection<Control.Noticias> noticias { get; set; }
            public ObservableCollection<Control.Noticias> Noticias
            {
                get{return noticias;}
                set
                {
                    noticias = value;
                    NotifyPropertyChanged("Noticias");
                }
            }
            IUserDialogs Dialogs;
            public string id;
            public int page = 1;
        #endregion

        public MainViewModel(string id)
        {
            Dialogs = UserDialogs.Instance;
            this.id = id;

            if (Device.RuntimePlatform == Device.Android)
            {
                PlatformSpecificImage = ImageSource.FromResource("Notigram.Resource.location_a.png");
            }
            else
            {
                PlatformSpecificImage = ImageSource.FromResource("Notigram.Resource.location.png");
            }
        }

        public async void GetNoticias()
        {
            if (isBusy) {
                return;
            }

            isBusy = true;
            using (Dialogs.Loading("Cargando", null, null, true, MaskType.Black))
            { 
                ObservableCollection<Control.Noticias> n = new ObservableCollection<Control.Noticias>();

                var json = await new Models.Service().GetNoticiasHttp(this.id, "1");

                for (int i = 0; i < json.Count; i++)
                {
                    //-9748590

                    List<Control.Category> category = JsonConvert.DeserializeObject<List<Control.Category>>(json[i]._embedded.GetValue("wp:term")[0].ToString());
                    DateTime date = DateTime.Parse(json[i].date);
                    DateTime hour = DateTime.Now;
                    DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("es-ES", false).DateTimeFormat;
                    var author = JsonConvert.DeserializeObject<Control.AuthorName>(json[i]._embedded.GetValue("author")[0].ToString());
                    var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>(json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                    //Debug.WriteLine("autor " +json[i].id);
                    //Debug.WriteLine(json[i]._embedded.GetValue("wp:term")[1].ToString());
                    var tags = JsonConvert.DeserializeObject<List<Control.CiudadesReporte>>(json[i]._embedded.GetValue("wp:term")[1].ToString());
                    string tag = "";
                    int count = tags.Count >= 3 ? 3 : tags.Count;
                    for(int y = 0; y < count; y++)
                    {
                        tag = tag+" #" + tags[y].name.Replace(" ","");
                    }
                    
                    string url = "";
                    try
                    {
                        var full = JsonConvert.DeserializeObject<Control.Image>(media_details.media_details.GetValue("sizes").ToString());
                        try
                        {
                            url = AsignUrl(full.medium.GetValue("source_url").ToString());
                        }
                        catch(Exception e)
                        {
                             url = AsignUrl(full.medium_large.GetValue("source_url").ToString());
                        }
                        //Console.WriteLine(author.name);
                        //AsignUrl(full.full.GetValue("source_url").ToString());
                        //Console.WriteLine("hora nota"+date.TimeOfDay);
                        //Console.WriteLine("hora celular" + hour.TimeOfDay);
                        //Console.WriteLine("hora restada "+(hour-date).Days+":"+(hour-date).Minutes);
                        string fecha="";
                        if ((hour - date).Days == 0 & (hour - date).Hours == 0 & (hour - date).Minutes >= 0)
                        {
                            fecha = (hour - date).Minutes == 1 || (hour - date).Minutes == 0  ? "Ahora" : (hour - date).Minutes + " minutos";
                        }else if ((hour - date).Days == 0 & (hour - date).Hours >= 0 & (hour - date).Minutes >= 0)
                        {
                            fecha = (hour - date).Hours == 1 ? (hour - date).Hours + " hora" : (hour - date).Hours + " horas";
                        }else if ((hour - date).Days >= 1 & (hour - date).Days < 2)
                        {
                            fecha = "Ayer";
                        }else if ((hour - date).Days >= 2)
                        {
                            fecha= date.Month + "/" + date.Day + "/" + date.Year;
                        }
                        n.Add(new Control.Noticias
                        {
                            url_image = ImageSource.FromUri(new Uri(url)),
                            category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                            tag = tag,
                            //slug = "@" + author.slug,
                            url_avatar = author.avatar_urls.GetValue("96").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                            //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                            date = fecha,
                            link = json[0].link,
                            id = json[0].id,
                            nameAuthor = author.name,
                            content = json[i].content,
                            noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                            user_meta = json[i].user_meta,
                            videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                            facebook = json[i].user_meta.GetValue("facebook").ToString()==""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                        }) ;
                    }
                    catch (Exception e)
                    {
                        string fecha = "";
                        if ((hour - date).Days == 0 & (hour - date).Hours == 0 & (hour - date).Minutes >= 0)
                        {
                            fecha = (hour - date).Minutes == 1 ? (hour - date).Minutes + " minuto" : (hour - date).Minutes + " minutos";
                        }
                        else if ((hour - date).Days == 0 & (hour - date).Hours >= 0 & (hour - date).Minutes >= 0)
                        {
                            fecha = (hour - date).Hours == 1 ? (hour - date).Hours + " hora" : (hour - date).Hours + " horas";
                        }
                        else if ((hour - date).Days >= 1 & (hour - date).Days < 2)
                        {
                            fecha = "Ayer";
                        }
                        else if ((hour - date).Days >= 2)
                        {
                            fecha = date.Month + "/" + date.Day + "/" + date.Year;
                        }
                        if (string.IsNullOrEmpty(url))
                        {
                            n.Add(new Control.Noticias
                            {
                                url_image = ImageSource.FromResource("Notigram.Resource.notigram_no_image.jpg", typeof(Constant.Images).GetTypeInfo().Assembly),
                                category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                                tag = tag,
                                //slug = "@" + author.slug,
                                url_avatar = author.avatar_urls.GetValue("96").ToString(),
                                rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                                //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                                date = fecha,
                                link = json[0].link,
                                id = json[0].id,
                                nameAuthor = author.name,
                                content = json[i].content,
                                noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                                user_meta = json[i].user_meta,
                                videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                                facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                            });
                        }
                        else {
                            n.Add(new Control.Noticias
                            {
                                url_image = ImageSource.FromResource(url, typeof(Constant.Images).GetTypeInfo().Assembly),
                                category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                                tag = tag,
                                //slug = "@" + author.slug,
                                url_avatar = author.avatar_urls.GetValue("96").ToString(),
                                rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                                //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                                date = fecha,
                                link = json[0].link,
                                id = json[0].id,
                                nameAuthor = author.name,
                                content = json[i].content,
                                noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                                user_meta = json[i].user_meta,
                                videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                                facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                            });
                        }
                        
                    }

                }
               
                Noticias = n;

            }
            isBusy = false;
        }

        private string AsignUrl(string url)
        {
            //Console.WriteLine(url.Substring(0, (url.Length - 5 )));
            if (url.EndsWith(".webp"))
            {
                return url.Substring(0, (url.Length - 5));
            }
            else
                return url;
            //Console.WriteLine(url.Replace(".webp", ""));
        }

        public async Task GetNewPage()
        {
            using (Dialogs.Loading("Nueva pagina", null, null, true, MaskType.Black))
            {
                var json = await new Models.Service().GetNoticiasHttp(this.id, page.ToString());
                for (int i = 0; i < json.Count; i++)
                {
                    //-9748590
                    var category = JsonConvert.DeserializeObject<List<Control.Category>>(json[i]._embedded.GetValue("wp:term")[0].ToString());
                    DateTime date = DateTime.Parse(json[i].date);
                    DateTime hour = DateTime.Now;
                    DateTimeFormatInfo dateTimeFormatInfo = new CultureInfo("es-ES", false).DateTimeFormat;
                    var author = JsonConvert.DeserializeObject<Control.AuthorName>(json[i]._embedded.GetValue("author")[0].ToString());
                    var media_details = JsonConvert.DeserializeObject<Control.FeaturedMedia>(json[i]._embedded.GetValue("wp:featuredmedia")[0].ToString());
                    var tags = JsonConvert.DeserializeObject<List<Control.CiudadesReporte>>(json[i]._embedded.GetValue("wp:term")[1].ToString());
                    string tag = "";
                    int count = tags.Count >= 3 ? 3 : tags.Count;
                    for (int y = 0; y < count; y++)
                    {
                        tag = tag + " #" + tags[y].name.Replace(" ", "");
                    }
                    string url = "";
                    string fecha = "";
                    if ((hour - date).Days == 0 & (hour - date).Hours == 0 & (hour - date).Minutes >= 0)
                    {
                        fecha = (hour - date).Minutes == 1 ? (hour - date).Minutes + " minuto" : (hour - date).Minutes + " minutos";
                    }
                    else if ((hour - date).Days == 0 & (hour - date).Hours >= 0 & (hour - date).Minutes >= 0)
                    {
                        fecha = (hour - date).Hours == 1 ? (hour - date).Hours + " hora" : (hour - date).Hours + " horas";
                    }
                    else if ((hour - date).Days >= 1 & (hour - date).Days < 2)
                    {
                        fecha = "Ayer";
                    }
                    else if ((hour - date).Days >= 2)
                    {
                        fecha = date.Month + "/" + date.Day + "/" + date.Year;
                    }
                    try
                    {
                        
                        var full = JsonConvert.DeserializeObject<Control.Image>(media_details.media_details.GetValue("sizes").ToString());
                        try
                        {
                            url = AsignUrl(full.medium_large.GetValue("source_url").ToString());
                        }
                        catch (Exception e)
                        {
                            url = AsignUrl(full.medium.GetValue("source_url").ToString());
                        }

                        Noticias.Add(new Control.Noticias
                        {
                            url_image = url,
                            category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                            tag = tag,
                            //slug = "@" + author.slug,
                            url_avatar = author.avatar_urls.GetValue("96").ToString(),
                            rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                            //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                            date = fecha,
                            link = json[0].link,
                            id = json[0].id,
                            nameAuthor = author.name,
                            content = json[i].content,
                            noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                            user_meta = json[i].user_meta,
                            videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                            facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                            instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                        });
                    }
                    catch (Exception e)
                    {

                        if (string.IsNullOrEmpty(url)) {
                            Noticias.Add(new Control.Noticias
                            {
                                url_image = ImageSource.FromResource("Notigram.Resource.notigram_no_image.jpg", typeof(Constant.Images).GetTypeInfo().Assembly),
                                category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                                tag = tag,
                                //slug = "@" + author.slug,
                                url_avatar = author.avatar_urls.GetValue("96").ToString(),
                                rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                                //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                                date = fecha,
                                link = json[0].link,
                                id = json[0].id,
                                nameAuthor = author.name,
                                content = json[i].content,
                                noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                                user_meta = json[i].user_meta,
                                videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                                facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                            });
                        }
                        else {
                            Noticias.Add(new Control.Noticias
                            {
                                url_image = ImageSource.FromResource(url, typeof(Constant.Images).GetTypeInfo().Assembly),
                                category = category[0].name.ToString().Substring(0, 1).Equals("-") ? category[1].name.ToString() : category[0].name.ToString(),
                                tag = tag,
                                //slug = "@" + author.slug,
                                url_avatar = author.avatar_urls.GetValue("96").ToString(),
                                rendered = json[i].title.GetValue("rendered").ToString().Replace("&#8220;", "\"").Replace("&#8221;", "\"").Replace("&#8216;", "\'").Replace("&#8217;", "\'"),
                                //date = date.Month + "/" + date.Day + "/" + date.Year + "\n" + date.TimeOfDay.ToString().Substring(0, 5),
                                date = fecha,
                                link = json[0].link,
                                id = json[0].id,
                                nameAuthor = author.name,
                                content = json[i].content,
                                noticieros = json[i].user_meta.GetValue("noticiero").ToString(),
                                user_meta = json[i].user_meta,
                                videoFb = json[i].video_fb[0].ToString().Equals("") ? null : json[i].video_fb[0].ToString(),
                                facebook = json[i].user_meta.GetValue("facebook").ToString() == ""
                                       ? null : ImageSource.FromResource("Notigram.Resource.facebook.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                twitter = json[i].user_meta.GetValue("twitter").ToString() == ""
                                      ? null : ImageSource.FromResource("Notigram.Resource.twitter.png", typeof(Constant.Images).GetTypeInfo().Assembly),
                                instagram = json[i].user_meta.GetValue("instagram").ToString() == ""
                                        ? null : ImageSource.FromResource("Notigram.Resource.instagram.png", typeof(Constant.Images).GetTypeInfo().Assembly)
                            });
                        }
                    }

                }
            }
        }

    }
}
