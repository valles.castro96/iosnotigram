﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Notigram.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Notigram.Models.WebViewRender), typeof(ExtendedWebViewRenderer))]
namespace Notigram.iOS.Renders
{
    public class ExtendedWebViewRenderer : WebViewRenderer
    {
        protected override void OnElementChanged(VisualElementChangedEventArgs e)
        {
            base.OnElementChanged(e);
            Delegate = new ExtendedUIWebViewDelegate(this);
        }
    }

    public class ExtendedUIWebViewDelegate : UIWebViewDelegate
    {
        private ExtendedWebViewRenderer extendedWebViewRenderer;

        public ExtendedUIWebViewDelegate(ExtendedWebViewRenderer extendedWebViewRenderer = null)
        {
            this.extendedWebViewRenderer = extendedWebViewRenderer ?? new ExtendedWebViewRenderer();
        }
        public override async void LoadingFinished(UIWebView webView)
        {
            var wv = extendedWebViewRenderer.Element as Notigram.Models.WebViewRender;
            if (wv != null)
            {
                await System.Threading.Tasks.Task.Delay(100);
                wv.HeightRequest = (double)webView.ScrollView.ContentSize.Height;
            }
        }
    }
}