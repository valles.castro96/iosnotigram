﻿using System;
using Notigram.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;

[assembly: ExportRenderer(typeof(Notigram.Models.ListViewRender), typeof(ListViewRenderIOS))]
namespace Notigram.iOS.Renders
{

    public class ListViewRenderIOS : ListViewRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                // Unsubscribe from event handlers and cleanup any resources
            }

            if (e.NewElement != null)
            {
                // Configure the native control and subscribe to event handlers
                Element.SeparatorColor = Color.Transparent;
                Element.SeparatorVisibility = SeparatorVisibility.None;
            }
        }
    }
}
