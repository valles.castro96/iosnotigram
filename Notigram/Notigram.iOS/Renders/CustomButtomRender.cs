﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Foundation;
using Notigram.iOS.Renders;
using UIKit;
using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Notigram.Control;

[assembly: ExportRenderer(typeof(Notigram.Models.ButtomRender), typeof(CustomButtomRender))]

namespace Notigram.iOS.Renders
{
    public class CustomButtomRender : ButtonRenderer
    {
        protected override void OnElementChanged(ElementChangedEventArgs<Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null) 
            {
                Control.Layer.BackgroundColor = Color.Transparent.ToCGColor();
            }
            
        }
    }
}