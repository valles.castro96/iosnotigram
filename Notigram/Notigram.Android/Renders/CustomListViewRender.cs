﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer(typeof(Notigram.Models.ListViewRender), typeof(Notigram.Droid.Renders.CustomListViewRender))]
namespace Notigram.Droid.Renders
{
    public class CustomListViewRender : ListViewRenderer
    {
        Context context;
        static Notigram.Models.ListViewRender listView = null;
        

        public CustomListViewRender(Context context) : base(context)
        {
            this.context = context;
        }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.ListView> e)
        {
            base.OnElementChanged(e);
        }
    }
}