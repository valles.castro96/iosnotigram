﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Webkit;
using Android.Widget;
using Notigram.Droid.Renders;
using Notigram.Models;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using WebView = Android.Webkit.WebView;

[assembly: ExportRenderer(typeof(Notigram.Models.WebViewRender), typeof(ExtendedWebViewRenderer))]

namespace Notigram.Droid.Renders
{
    public class ExtendedWebViewRenderer : WebViewRenderer
    {
        static Notigram.Models.WebViewRender webViewRender = null;
        WebView webView;
        public ExtendedWebViewRenderer(Context context) : base(context) { }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.WebView> e)
        {
            base.OnElementChanged(e);
            webViewRender = e.NewElement as Notigram.Models.WebViewRender;
            webView = Control;
            if(e.OldElement == null)
            {
                webView.SetWebViewClient(new ExtendedWebViewClient());
            }
        }

        class ExtendedWebViewClient : Android.Webkit.WebViewClient
        {
            public override async void  OnPageFinished(WebView view, string url)
            {
                if (webViewRender != null)
                {
                    int i = 10;
                    while (view.ContentHeight == 0 && i-- > 0)
                        await System.Threading.Tasks.Task.Delay(100);
                    webViewRender.HeightRequest = view.ContentHeight*1.3;
                    //(webViewRender.Parent.Parent as ViewCell).ForceUpdateSize();
                }
                base.OnPageFinished(view, url);
            }
        }
    }
}