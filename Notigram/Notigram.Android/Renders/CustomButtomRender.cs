﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Notigram.Droid.Renders;
using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using Button = Xamarin.Forms.Button;

[assembly: ExportRenderer(typeof(Notigram.Models.ButtomRender), typeof(CustomButtomRender))]
namespace Notigram.Droid.Renders
{
    class CustomButtomRender : Xamarin.Forms.Platform.Android.AppCompat.ButtonRenderer
    {
        public CustomButtomRender(Context context) : base(context) { }
        protected override void OnElementChanged(ElementChangedEventArgs<Xamarin.Forms.Button> e)
        {
            base.OnElementChanged(e);
            if (e.OldElement == null) 
            {
                Control.SetShadowLayer(0, 0, 0,Android.Graphics.Color.Black);
            }
        }
    }
}